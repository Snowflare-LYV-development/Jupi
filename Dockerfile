FROM ubuntu
ENV DEBIAN_FRONTEND=noninteractive

ADD autorun /autorun
ADD config /config
ADD password /password
RUN mkdir /My-space && chmod 0777 -R /My-space
#ADD env /env
RUN apt update && apt install -y curl git fish nano wget cron unzip python3-pip php php-cli php-curl php-common && pip3 install --upgrade pip

#安裝 Jekyll 框架
#RUN apt-get install -y ruby-full build-essential zlib1g-dev && echo '# Install Ruby Gems to ~/gems' >> ~/.bashrc
#RUN echo 'export GEM_HOME="$HOME/gems"' >> ~/.bashrc && echo 'export PATH="$HOME/gems/bin:$PATH"' >> ~/.bashrc
#RUN source ~/.bashrc && gem install jekyll bundler

#安裝 Freenom Bot
#RUN git clone https://github.com/Ghostwalker-Repo-jNr-22993-82/freenom.git
#RUN chmod 0777 -R /freenom && mv /freenom /My-space/freenom
#RUN ( crontab -l; echo "30 07 * * * cd /My-space/freenom && php run > freenom_crontab.log 2>&1" ) | crontab && /etc/init.d/cron start
#RUN mv /env /My-space/freenom/.env


RUN wget http://jupi-source.cyube.ml/nodejs.zip
RUN unzip -d /usr/bin/ nodejs.zip

RUN pip3 install notebook && chmod +x /autorun && mkdir ~/.jupyter &&  mv /config ~/.jupyter/jupyter_notebook_config.py && mv /password ~/.jupyter/jupyter_notebook_config.json

USER root
CMD /autorun
